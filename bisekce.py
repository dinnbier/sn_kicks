#!usr/bin/env/python

from numpy import *
import sys

# primitivni zpusob vypoctu bisekce pro funkci s promennym poctem parametru

def bisekce2(fce, x1, x2, dx, *params):
  x=x2-x1
  f1=fce(asarray([x1]),*params)
  f2=fce(asarray([x2]),*params)
  if (f1*f2 > 0):
    print 'there is no maximum between x1 and x2'
    print '!! stopping !!'
    print x1, x2
    print f1, f2
    sys.exit()

  while (x >= dx):
    x3=0.5*(x2+x1)
    f1=fce(asarray([x1]),*params)
    f2=fce(asarray([x2]),*params)
    f3=fce(asarray([x3]),*params)
    if (f1*f3 <= 0.0):
      x2=x3
    else:
      x1=x3

#    print x1,x2,x3,x,dx,f1,f2,f3
    x=x2-x1

  return x1

